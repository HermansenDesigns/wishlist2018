# Kaspers Ønskeliste Jul 2019

- Seagate IronWolf NAS HDD
  - [Link](https://www.proshop.dk/Harddisk/Seagate-IronWolf-ST6000VN0033-Harddisk-6-TB-35-7200-rpm-SATA-600-256-MB-cache/2622656)
- Pull up bar
  - [Link](https://www.billig-fitness.dk/collections/pull-up-bar/products/double-handed-chin-up-bar)
- Vægte
  - [Link](https://www.fitnessgruppen.dk/10-kg-justerbar-haandvaegt-30-mm/)
- Raspberry pi v4 4gb ram
  - [Link](https://raspberrypi.dk/en/product/raspberry-pi-4-model-b/)
- Raspberry Pi Zero w (Only Raspberry Pi Zero W)
  - [Link](https://raspberrypi.dk/en/product/raspberry-pi-zero-w/)
- Løbeudstyr
  - [Link](https://www.loberen.dk/products/fusion-mens-s100-run-vest-3)
  - Reflekser
- Hævesænke bord 200x100cm
- Skægtrimmer
  - [Eksempel](https://www.made4men.dk/remington-mb4110-skaegtrimmer.html?gclid=Cj0KCQiAoIPvBRDgARIsAHsCw08SfgDdy9ModkdCqfEB0mb3K_vKqQfbwdSXLl6odqSUA4Yz3mWdv9saAnWVEALw_wcB)
- LED strip med justerbar RGB farver
  - [Link](https://ardustore.dk/produkt/ws2812b-5050-rgb-led-strip-5v-dc)
- Gaming headset
  - Eksempelvis 
    - Logitech G635 eller G935
- Skorjter i simple farver eller print størrelse XL
- Simpel metal pung til kort (F.eks. ridge wallet).
- Headset amp
  - [Link](https://drop.com/buy/massdrop-o2-sdac-dac-amp?clickid=wsGX3q0lFxyOWKYwUx0Mo3ERUkn1znz81TJm3w0&irgwc=1&utm_term=252901&utm_content=Skimbit%20Ltd.&utm_medium=affiliate&utm_source=impactradius&utm_placement=soundguys.com&utm_keyword=&mode=shop_open)
- Holder til headset
  - [Link](https://www.evermart.dk/carrier-headset-holder?gclid=Cj0KCQiAoIPvBRDgARIsAHsCw082-tZQJP5UHDN4c8NIuclTH0G9xrAy_3lRJv_sTP74ox7NFN_MO-IaAulvEALw_wcB)
- Samsung Galaxy Tab S6/Samsung Galaxy Tab S4 2018 edition (Gerne i mørke farver)
  - [Link](https://www.samsung.com/global/galaxy/galaxy-tab-s6/)
